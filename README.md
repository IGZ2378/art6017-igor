PROJET de l'atellier art6017:

Le but de mon projet était de créer un site internet pour mon collectif de musique.
Le site se devait de répondre à trois besoins réels :
 - présenter visuellement le travail du collectif.
 - présenter les prestations du collectif.
 - renvoyer via des liens vers les réseaux sociaux et plateformes d'écoutes.
 
 Je suis donc parvenu à réaliser une page d'accueil simple réunissant les prestations,
 une présentation ainsi que les liens.
 
 En tout j'ai utilisés trois templates que j'ai modifié pour subvenir à mes besoins :
 - le template "People Portfolio II Template" (pour les pages "collectif" et "prestations")
 - le template "Startup Template" (pour la page "index")
 - le template "Marketing / Website Template" (pour une partie de la page "prestation")
 
 Le site possède également une sidebar qui permet à tout moment de se rendre sur toutes les pages du site.
 
 J'ai atteint mes objectifs pour ce projet, mais mon besoin étant réel je pense continuer à développer le site et j'ai déja réservé le nom de domaine 
 qui renverra sur le site. J'avais déja auparavant créé des blogs ou sites, toujours via des interfaces (principalement WordPress), mais dans
 ce cas le développement simple en HTML + CSS me parait plus direct et approprié.
 
 